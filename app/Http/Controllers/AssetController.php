<?php

namespace App\Http\Controllers;

use App\Asset;
use Illuminate\Http\Request;
use App\Category;

class AssetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
/*        $this->authorize('create', Asset::class);
        $assets = Asset::orderBy('created_at', 'desc')->paginate(10);
        return view('assets.index')->with('assets', $assets);*/
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('create', Asset::class);
        $categories = Category::all();
        return view('assets.create')->with('categories', $categories);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {



        /*dd($request->input('category'));*/
        $this->authorize('create', Asset::class);

        $request->validate([
                     'category' => 'required|Integer',
                     'serial_number' => 'required|unique:assets,serial_number'
        ]);

        $category = htmlspecialchars($request->input('category'));
        $serial_number = htmlspecialchars($request->input('serial_number'));


        $asset = new Asset;

        $asset->serial_number = $serial_number;
        $asset->category_id = $category;

        $asset->save();

        return redirect('categories/' .$category)->with('success', 'Successfully added a new asset.');





    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Asset  $asset
     * @return \Illuminate\Http\Response
     */
    public function show(Asset $asset)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Asset  $asset
     * @return \Illuminate\Http\Response
     */
    public function edit(Asset $asset)
    {
        $this->authorize('create', Asset::class);
        $categories = Category::all();
        return view('assets.edit')->with('categories', $categories)->with('asset', $asset);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Asset  $asset
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Asset $asset)
    {
        $this->authorize('update', Asset::class);

        $request->validate([
                     'category' => 'required|Integer',
                     'serial_number' => 'required|unique:assets,serial_number'
        ]);

        $category = htmlspecialchars($request->input('category'));
        $serial_number = htmlspecialchars($request->input('serial_number'));

        $asset->serial_number = $serial_number;
        $asset->category_id = $category;

        $asset->save();

        return redirect('/categories/' .$category)->with('success', 'Successfully updated asset.');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Asset  $asset
     * @return \Illuminate\Http\Response
     */
    public function destroy(Asset $asset)
    {
           if($asset->isAvailable == 1){
                $asset->isAvailable = 0;
           }else{
                $asset->isAvailable = 1;
           }

           $asset->save();


           return back();
    }
}
