<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Asset extends Model
{
    //eloquent relationship // many to one // assets to categories
    public function category()
    {
        return $this->belongsTo('\App\Category');
    }

    //eloquent relationship // one to many // assets to transactions
    public function transactions()
    {
        return $this->hasMany('\App\Transactions');
    }
}
