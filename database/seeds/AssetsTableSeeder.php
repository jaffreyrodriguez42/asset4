<?php

use Illuminate\Database\Seeder;

class AssetsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('assets')->delete();
        
        \DB::table('assets')->insert(array (
            0 => 
            array (
                'id' => 1,
                'isAvailable' => 1,
                'serial_number' => 'E-EBG1',
                'created_at' => '2020-05-01 13:06:19',
                'updated_at' => '2020-05-01 13:11:20',
                'category_id' => 1,
            ),
            1 => 
            array (
                'id' => 2,
                'isAvailable' => 1,
                'serial_number' => 'E-EBG2',
                'created_at' => '2020-05-02 03:58:44',
                'updated_at' => '2020-05-02 03:58:44',
                'category_id' => 2,
            ),
            2 => 
            array (
                'id' => 3,
                'isAvailable' => 1,
                'serial_number' => 'E-EBXGA3LCD',
                'created_at' => '2020-05-04 11:43:16',
                'updated_at' => '2020-05-04 11:43:16',
                'category_id' => 3,
            ),
            3 => 
            array (
                'id' => 4,
                'isAvailable' => 1,
                'serial_number' => 'E-EBXGA3LCD1',
                'created_at' => '2020-05-04 11:45:53',
                'updated_at' => '2020-05-04 11:45:53',
                'category_id' => 3,
            ),
            4 => 
            array (
                'id' => 5,
                'isAvailable' => 1,
                'serial_number' => 'E-EBXGA3LCD2',
                'created_at' => '2020-05-04 11:46:12',
                'updated_at' => '2020-05-04 11:46:12',
                'category_id' => 3,
            ),
            5 => 
            array (
                'id' => 6,
                'isAvailable' => 1,
                'serial_number' => 'E-EBG3',
                'created_at' => '2020-05-04 11:47:30',
                'updated_at' => '2020-05-04 11:47:30',
                'category_id' => 2,
            ),
            6 => 
            array (
                'id' => 7,
                'isAvailable' => 1,
                'serial_number' => 'E-EBL2',
                'created_at' => '2020-05-04 11:48:19',
                'updated_at' => '2020-05-04 11:48:19',
                'category_id' => 1,
            ),
            7 => 
            array (
                'id' => 8,
                'isAvailable' => 1,
                'serial_number' => 'E-EW1',
                'created_at' => '2020-05-04 11:50:29',
                'updated_at' => '2020-05-04 11:50:29',
                'category_id' => 4,
            ),
            8 => 
            array (
                'id' => 9,
                'isAvailable' => 1,
                'serial_number' => 'E-EW2',
                'created_at' => '2020-05-04 11:50:48',
                'updated_at' => '2020-05-04 11:50:48',
                'category_id' => 4,
            ),
            9 => 
            array (
                'id' => 10,
                'isAvailable' => 1,
                'serial_number' => 'E-EW3',
                'created_at' => '2020-05-04 11:51:06',
                'updated_at' => '2020-05-04 11:51:06',
                'category_id' => 4,
            ),
            10 => 
            array (
                'id' => 11,
                'isAvailable' => 1,
                'serial_number' => 'E-EW4',
                'created_at' => '2020-05-04 11:54:10',
                'updated_at' => '2020-05-04 11:54:10',
                'category_id' => 4,
            ),
            11 => 
            array (
                'id' => 12,
                'isAvailable' => 1,
                'serial_number' => 'E-EW5',
                'created_at' => '2020-05-04 11:56:17',
                'updated_at' => '2020-05-04 11:56:17',
                'category_id' => 4,
            ),
            12 => 
            array (
                'id' => 13,
                'isAvailable' => 1,
                'serial_number' => 'E-EW6',
                'created_at' => '2020-05-04 11:57:16',
                'updated_at' => '2020-05-04 11:57:16',
                'category_id' => 4,
            ),
            13 => 
            array (
                'id' => 14,
                'isAvailable' => 1,
                'serial_number' => 'E-ETP1',
                'created_at' => '2020-05-04 12:00:12',
                'updated_at' => '2020-05-04 12:00:12',
                'category_id' => 5,
            ),
            14 => 
            array (
                'id' => 15,
                'isAvailable' => 1,
                'serial_number' => 'E-ETP2',
                'created_at' => '2020-05-04 12:00:22',
                'updated_at' => '2020-05-04 12:00:22',
                'category_id' => 5,
            ),
            15 => 
            array (
                'id' => 16,
                'isAvailable' => 1,
                'serial_number' => 'E-ETP3',
                'created_at' => '2020-05-04 12:00:30',
                'updated_at' => '2020-05-04 12:00:30',
                'category_id' => 5,
            ),
            16 => 
            array (
                'id' => 17,
                'isAvailable' => 1,
                'serial_number' => 'E-EBWI2',
                'created_at' => '2020-05-04 12:03:14',
                'updated_at' => '2020-05-04 12:03:14',
                'category_id' => 6,
            ),
            17 => 
            array (
                'id' => 18,
                'isAvailable' => 1,
                'serial_number' => 'E-EBWI3',
                'created_at' => '2020-05-04 12:03:23',
                'updated_at' => '2020-05-04 12:03:23',
                'category_id' => 6,
            ),
            18 => 
            array (
                'id' => 19,
                'isAvailable' => 1,
                'serial_number' => 'E-EBWI4',
                'created_at' => '2020-05-04 12:03:32',
                'updated_at' => '2020-05-04 12:03:32',
                'category_id' => 6,
            ),
            19 => 
            array (
                'id' => 20,
                'isAvailable' => 1,
                'serial_number' => 'E-EBWW1',
                'created_at' => '2020-05-04 12:08:54',
                'updated_at' => '2020-05-04 12:08:54',
                'category_id' => 7,
            ),
            20 => 
            array (
                'id' => 21,
                'isAvailable' => 1,
                'serial_number' => 'E-EBWW2',
                'created_at' => '2020-05-04 12:09:01',
                'updated_at' => '2020-05-04 12:09:01',
                'category_id' => 7,
            ),
            21 => 
            array (
                'id' => 22,
                'isAvailable' => 1,
                'serial_number' => 'E-EBWW3',
                'created_at' => '2020-05-04 12:09:08',
                'updated_at' => '2020-05-04 12:09:08',
                'category_id' => 7,
            ),
            22 => 
            array (
                'id' => 23,
                'isAvailable' => 1,
                'serial_number' => 'E-EBNL1',
                'created_at' => '2020-05-04 12:11:21',
                'updated_at' => '2020-05-04 12:11:21',
                'category_id' => 8,
            ),
            23 => 
            array (
                'id' => 24,
                'isAvailable' => 1,
                'serial_number' => 'E-EBNL2',
                'created_at' => '2020-05-04 12:11:27',
                'updated_at' => '2020-05-04 12:11:27',
                'category_id' => 8,
            ),
            24 => 
            array (
                'id' => 25,
                'isAvailable' => 1,
                'serial_number' => 'E-EBNL3',
                'created_at' => '2020-05-04 12:11:35',
                'updated_at' => '2020-05-04 12:11:35',
                'category_id' => 8,
            ),
        ));
        
        
    }
}