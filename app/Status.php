<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    //eloquent relationship // one to many // statuses to transactions
    public function transactions()
    {
        return $this->hasMany('\App\Transactions');
    }
}
