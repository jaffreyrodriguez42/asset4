<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('categories')->delete();
        
        \DB::table('categories')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Epson EB-L1100',
                'description' => 'Nice Projector',
                'img_path' => 'images/1588338370.png',
                'isActive' => 1,
                'created_at' => '2020-05-01 13:06:10',
                'updated_at' => '2020-05-01 13:06:10',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Epson EB-2042',
                'description' => 'Corporate and Education',
                'img_path' => 'images/1588591216.jpg',
                'isActive' => 1,
                'created_at' => '2020-05-02 03:58:15',
                'updated_at' => '2020-05-04 11:21:16',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Epson EB-2055',
                'description' => 'High Quality Connectivity',
                'img_path' => 'images/1588593997.jpg',
                'isActive' => 1,
                'created_at' => '2020-05-04 11:25:12',
                'updated_at' => '2020-05-04 12:06:37',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'Epson EB-1780W',
                'description' => 'Wireless Projector',
                'img_path' => 'images/1588592988.png',
                'isActive' => 1,
                'created_at' => '2020-05-04 11:49:48',
                'updated_at' => '2020-05-04 11:49:48',
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'Epson EB-1785W',
                'description' => 'Ultra-Thin Projector',
                'img_path' => 'images/1588593572.png',
                'isActive' => 1,
                'created_at' => '2020-05-04 11:59:32',
                'updated_at' => '2020-05-04 11:59:32',
            ),
            5 => 
            array (
                'id' => 6,
                'name' => 'Epson-EB-536Wi',
                'description' => 'Interactive Short Throw Projectors',
                'img_path' => 'images/1588593766.png',
                'isActive' => 1,
                'created_at' => '2020-05-04 12:02:46',
                'updated_at' => '2020-05-04 12:02:46',
            ),
            6 => 
            array (
                'id' => 7,
                'name' => 'Epson EB-685Wi',
                'description' => 'Ultra-Short Throw',
                'img_path' => 'images/1588594102.png',
                'isActive' => 1,
                'created_at' => '2020-05-04 12:08:22',
                'updated_at' => '2020-05-04 12:08:22',
            ),
            7 => 
            array (
                'id' => 8,
                'name' => 'Epson-EB-L1070UNL',
                'description' => 'Borderless Multi Projection',
                'img_path' => 'images/1588594237.jpg',
                'isActive' => 1,
                'created_at' => '2020-05-04 12:10:37',
                'updated_at' => '2020-05-04 12:10:37',
            ),
        ));
        
        
    }
}