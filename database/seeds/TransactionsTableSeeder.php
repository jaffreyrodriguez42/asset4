<?php

use Illuminate\Database\Seeder;

class TransactionsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('transactions')->delete();
        
        \DB::table('transactions')->insert(array (
            0 => 
            array (
                'id' => 2,
                'user_id' => 2,
                'status_id' => 4,
                'category_id' => 1,
                'asset_id' => 1,
                'borrowDate' => '2020-05-22 00:00:00',
                'returnDate' => '2020-05-29 00:00:00',
                'created_at' => '2020-05-01 13:10:55',
                'updated_at' => '2020-05-01 13:11:20',
            ),
            1 => 
            array (
                'id' => 3,
                'user_id' => 2,
                'status_id' => 3,
                'category_id' => 8,
                'asset_id' => NULL,
                'borrowDate' => '2020-05-06 00:00:00',
                'returnDate' => '2020-05-20 00:00:00',
                'created_at' => '2020-05-04 12:12:04',
                'updated_at' => '2020-05-04 12:12:09',
            ),
            2 => 
            array (
                'id' => 4,
                'user_id' => 4,
                'status_id' => 3,
                'category_id' => 8,
                'asset_id' => NULL,
                'borrowDate' => '2020-05-05 00:00:00',
                'returnDate' => '2020-05-20 00:00:00',
                'created_at' => '2020-05-05 02:32:29',
                'updated_at' => '2020-05-05 02:33:51',
            ),
            3 => 
            array (
                'id' => 5,
                'user_id' => 4,
                'status_id' => 1,
                'category_id' => 7,
                'asset_id' => NULL,
                'borrowDate' => '2020-05-14 00:00:00',
                'returnDate' => '2020-05-27 00:00:00',
                'created_at' => '2020-05-05 02:32:42',
                'updated_at' => '2020-05-05 02:32:42',
            ),
            4 => 
            array (
                'id' => 6,
                'user_id' => 4,
                'status_id' => 4,
                'category_id' => 6,
                'asset_id' => 17,
                'borrowDate' => '2020-05-27 00:00:00',
                'returnDate' => '2020-05-26 00:00:00',
                'created_at' => '2020-05-05 02:32:51',
                'updated_at' => '2020-05-05 02:34:44',
            ),
            5 => 
            array (
                'id' => 7,
                'user_id' => 4,
                'status_id' => 3,
                'category_id' => 6,
                'asset_id' => NULL,
                'borrowDate' => '2020-05-27 00:00:00',
                'returnDate' => '2020-05-26 00:00:00',
                'created_at' => '2020-05-05 02:32:51',
                'updated_at' => '2020-05-05 02:33:56',
            ),
            6 => 
            array (
                'id' => 8,
                'user_id' => 4,
                'status_id' => 1,
                'category_id' => 5,
                'asset_id' => NULL,
                'borrowDate' => '2020-05-19 00:00:00',
                'returnDate' => '2020-05-27 00:00:00',
                'created_at' => '2020-05-05 02:32:58',
                'updated_at' => '2020-05-05 02:32:58',
            ),
            7 => 
            array (
                'id' => 9,
                'user_id' => 4,
                'status_id' => 5,
                'category_id' => 4,
                'asset_id' => NULL,
                'borrowDate' => '2020-05-08 00:00:00',
                'returnDate' => '2020-05-28 00:00:00',
                'created_at' => '2020-05-05 02:33:06',
                'updated_at' => '2020-05-05 02:34:31',
            ),
            8 => 
            array (
                'id' => 10,
                'user_id' => 4,
                'status_id' => 2,
                'category_id' => 3,
                'asset_id' => 3,
                'borrowDate' => '2020-05-20 00:00:00',
                'returnDate' => '2020-06-05 00:00:00',
                'created_at' => '2020-05-05 02:33:18',
                'updated_at' => '2020-05-05 02:35:04',
            ),
            9 => 
            array (
                'id' => 11,
                'user_id' => 4,
                'status_id' => 4,
                'category_id' => 2,
                'asset_id' => 2,
                'borrowDate' => '2020-05-27 00:00:00',
                'returnDate' => '2020-05-28 00:00:00',
                'created_at' => '2020-05-05 02:33:25',
                'updated_at' => '2020-05-05 02:34:57',
            ),
            10 => 
            array (
                'id' => 12,
                'user_id' => 4,
                'status_id' => 5,
                'category_id' => 2,
                'asset_id' => NULL,
                'borrowDate' => '2020-05-27 00:00:00',
                'returnDate' => '2020-05-28 00:00:00',
                'created_at' => '2020-05-05 02:33:25',
                'updated_at' => '2020-05-05 02:34:29',
            ),
            11 => 
            array (
                'id' => 13,
                'user_id' => 4,
                'status_id' => 3,
                'category_id' => 1,
                'asset_id' => NULL,
                'borrowDate' => '2020-05-20 00:00:00',
                'returnDate' => '2020-05-28 00:00:00',
                'created_at' => '2020-05-05 02:33:34',
                'updated_at' => '2020-05-05 02:34:08',
            ),
        ));
        
        
    }
}