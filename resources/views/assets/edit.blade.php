@extends('layouts.app')

@section('content')

<div class="card-header">
	Edit Asset
</div>

<div class="card-body">
	<form method="POST" action="/assets/ {{ $asset->id }} " enctype="multipart/form-data">
		@csrf
		@method('PUT')
		<div class="form-group">
			<label for="category">Product Lines: </label>
			<select class="form-control" id="category_id" name="category">
				<option>Select a product line:</option>
				@if(count($categories) > 0)
					@foreach($categories as $category)
						@if($asset->category_id == $category->id)
							<option value="{{ $category->id }}" selected>{{ $category->name }}</option>
						@else
							<option value="{{ $category->id }}">{{ $category->name }}</option>
						@endif
					@endforeach
				@endif
			</select>
			
		</div>
		<div class="form-group">
			<label for="description">Serial Number: </label>
			<input class="form-control" type="text" name="serial_number" id="description" value="{{ $asset->serial_number }}">
		</div>
		
		<button type="submit" class="btn btn-success">
			Update Asset
		</button>
	</form>
</div>

@endsection