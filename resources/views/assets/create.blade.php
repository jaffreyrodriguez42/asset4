@extends('layouts.app')

@section('content')

<div class="card-header">
	Add Asset
</div>

<div class="card-body">
	<form method="POST" action="/assets" enctype="multipart/form-data">
		@csrf
		<div class="form-group">
			<label for="name">Product Lines: </label>
			<select class="form-control" id="category_id" name="category">
				<option>Select a product line:</option>
				@if(count($categories) > 0)
					@foreach($categories as $category)
						<option value="{{ $category->id }}">{{ $category->name }}</option>
					@endforeach
				@endif
			</select>
			
		</div>
		<div class="form-group">
			<label for="description">Serial Number: </label>
			<input class="form-control" type="text" name="serial_number" id="description">
		</div>
		
		<button type="submit" class="btn btn-success">
			Save asset
		</button>
	</form>
</div>

@endsection