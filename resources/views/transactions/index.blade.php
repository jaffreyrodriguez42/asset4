@extends('layouts.app')

@section('content')

@can('role_id')
    <div class="card-header col-md-12">
        <h5>Pending Requests</h5>
    </div>

    <div class="card-body row col-md-12 custyle">
    <table class="table table-striped custab">
    <thead>
    
        <tr>
            <th class="text-center align-middle">Transaction No.</th>
            <th class="align-middle">Status</th>
            <th class="align-middle">Product Line</th>
         {{--    <th class="align-middle">Asset Serial No.</th> --}}
            <th class="align-middle">Borrow Date</th>
            <th class="align-middle">Return Date</th>
            <th class="text-center align-middle">Requested By</th>
            <th class="text-center align-middle" colspan="2">Action</th>
        </tr>
    </thead>
    <tbody>
    	@foreach($transactions as $transaction)
            @if($transaction->status_id == 1)
            <tr>
            	<td class="text-center align-middle"><a href="/transactions/{{$transaction->id}}">{{$transaction->id}}</a></td>
                <td class="align-middle">{{$transaction->status->name}}</td>
                <td class="align-middle">{{$transaction->category->name}}</td>
               {{--  <td class="text-center align-middle"></td> --}}
                <td class="align-middle">{{$transaction->borrowDate}}</td>
                <td class="align-middle">{{$transaction->returnDate}}</td>
                <td class="text-center align-middle">{{$transaction->user->name}}</td>
                

                

                <td colspan="4" class="text-center align-middle" >
                	<div class="d-inline-block">
                        <form method="POST" action="/transactions/{{$transaction->id}}">
                            @csrf
                            @method('PUT')

                           
                            {{-- <button type="submit" class="btn btn-danger">Deny</button>                          
                            
                            <button type="submit" class="btn btn-success">Approve</button> --}}

                            <input type="submit" name="deny" value="Deny" class="btn btn-danger">
                            <input type="submit" name="approve" value="Approve" class="btn btn-success">
                            
                        </form>
                    </div>

                </td>
            </tr>
            @endif
      	@endforeach
      </tbody>
    </table>
{{--    {{ $pending_transactions->render() }} --}}

    </div>


    {{-- approved requests --}}
     <div class="card-header col-md-12">
         <h5>Approved Requests</h5>
     </div>

     <div class="card-body row col-md-12 custyle">
     <table class="table table-striped custab">
     <thead>
     
         <tr>
             <th class="align-middle">Transaction No.</th>
             <th class="align-middle">Status</th>
             <th class="align-middle">Product Line</th>
             <th class="align-middle">Asset Serial No.</th>
             <th class="align-middle">Borrow Date</th>
             <th class="align-middle">Return Date</th>
             <th class="text-center align-middle">Requested By</th>
             <th class="text-center align-middle" colspan="2">Action</th>
         </tr>
     </thead>
     <tbody>
        @foreach($transactions as $transaction)
            @if($transaction->status_id == 2)
             <tr>
                 <td class="text-center align-middle"><a href="/transactions/{{$transaction->id}}">{{$transaction->id}}</a></td>
                 <td class="align-middle">{{$transaction->status->name}}</td>
                 <td class="align-middle">{{$transaction->category->name}}</td>
                 <td class="text-center align-middle">

                        {{ $transaction->asset->serial_number }}           
                 </td>
                 <td class="align-middle">{{$transaction->borrowDate}}</td>
                 <td class="align-middle">{{$transaction->returnDate}}</td>
                 <td class="text-center align-middle">{{$transaction->user->name}}</td>
                 

                 

                 <td colspan="4" class="text-center align-middle" >
                    <div class="d-inline-block">
                         <form method="POST" action="/transactions/{{$transaction->id}}">
                             @csrf
                             @method('PUT')


                             <input type="submit" name="return" value="Tag as returned" class="btn btn-success">
                             
                         </form>
                     </div>

                 </td>
             </tr>
             @endif
        @endforeach
       </tbody>
     </table>
  {{--   {{ $approved_transactions->render() }} --}}

     </div>


       {{-- completed requests --}}
        <div class="card-header col-md-12">
            <h5>Completed Transactions</h5>
        </div>

        <div class="card-body row col-md-12 custyle">
        <table class="table table-striped custab">
        <thead>
        
            <tr>
                <th class="text-center align-middle">Transaction No.</th>
                <th class="align-middle">Status</th>
                <th class="align-middle">Product Line</th>
                <th class="text-center align-middle">Asset Serial No.</th>
                <th class="align-middle">Borrow Date</th>
                <th class="align-middle">Return Date</th>
                <th class="text-center align-middle">Requested By</th>
             {{--    <th class="text-center align-middle" colspan="2">Action</th> --}}
            </tr>
        </thead>
        <tbody>
           @foreach($transactions as $transaction)
               @if($transaction->status_id == 5 || $transaction->status_id == 4)
                <tr>
                   <td class="text-center align-middle"><a href="/transactions/{{$transaction->id}}">{{$transaction->id}}</a></td>
                    <td class="align-middle">{{$transaction->status->name}}</td>
                    <td class="align-middle">{{$transaction->category->name}}</td>

                    <td class="text-center align-middle">
                        @if($transaction->status_id == 5)
                            {{ $na['na'] }}
                        @elseif($transaction->status_id == 4)
                            {{ $transaction->asset->serial_number }}
                        @endif

                    </td>
                    <td class="align-middle">{{$transaction->borrowDate}}</td>
                    <td class="align-middle">{{$transaction->returnDate}}</td>
                    <td class="text-center align-middle">{{$transaction->user->name}}</td>
                    

                    

                    {{-- <td colspan="4" class="text-center align-middle" >
                       <div class="d-inline-block">
                            <form method="POST" action="/transactions/{{$transaction->id}}">
                                @csrf
                                @method('PUT')

                               
                                <input type="submit" name="approve" value="Tag as returned" class="btn btn-success">
                                
                            </form>
                        </div>

                    </td> --}}
                </tr>
                @endif
           @endforeach
          </tbody>
        </table>
     {{--   {{ $completed_transactions->render() }} --}}

        </div>

@else
        {{-- non admin pending transaction page --}}

     <div class="card-header col-md-12">
         <h5>My Pending Transactions</h5>
     </div>

     <div class="card-body row col-md-12 custyle">
     <table class="table table-striped custab">
     <thead>
     
         <tr>
             <th class="text-center align-middle">Transaction No.</th>
           
             <th class="align-middle">Product Line</th>
          {{--    <th class="align-middle">Asset Serial No.</th> --}}
             <th class="align-middle">Borrow Date</th>
             <th class="align-middle">Return Date</th>
                
             <th class="text-center align-middle" colspan="2">Action</th>
         </tr>
     </thead>
     <tbody>
        @foreach($transactions as $transaction)
            @if($transaction->status_id == 1)
             <tr>
                <td class="text-center align-middle"><a href="/transactions/{{$transaction->id}}">{{$transaction->id}}</a></td>
           
                 <td class="align-middle">{{$transaction->category->name}}</td>
                {{--  <td class="text-center align-middle"></td> --}}
                 <td class="align-middle">{{$transaction->borrowDate}}</td>
                 <td class="align-middle">{{$transaction->returnDate}}</td>
            
                 

                 

                 <td colspan="4" class="text-center align-middle" >
                    <div class="d-inline-block">
                         <form method="POST" action="/transactions/{{$transaction->id}}">
                             @csrf
                             @method('PUT')
                            
                             <button type="submit" class="btn btn-danger" name="cancel" value="cancel">Cancel</button>
                           
                         </form>
                     </div>

                 </td>
             </tr>
             @endif
        @endforeach
       </tbody>
     </table>
    {{-- {{ $transactions->render() }} --}}

     </div>


     {{-- non admin approved requests --}}

      <div class="card-header col-md-12">
          <h5>My Approved Requests</h5>
      </div>

      <div class="card-body row col-md-12 custyle">
      <table class="table table-striped custab">
      <thead>
      
          <tr>
              <th class="text-center align-middle">Transaction No.</th>
            
              <th class="align-middle">Product Line</th>
              <th class="text-center align-middle">Asset Serial No.</th>
           {{--    <th class="align-middle">Asset Serial No.</th> --}}
              <th class="align-middle">Borrow Date</th>
              <th class="align-middle">Return Date</th>
                 
             {{--  <th class="text-center align-middle" colspan="2">Action</th> --}}
          </tr>
      </thead>
      <tbody>
         @foreach($transactions as $transaction)
             @if($transaction->status_id == 2)
              <tr>
                 <td class="text-center align-middle"><a href="/transactions/{{$transaction->id}}">{{$transaction->id}}</a></td>
            
                  <td class="align-middle">{{$transaction->category->name}}</td>
                 {{--  <td class="text-center align-middle"></td> --}}
                 <td class="text-center align-middle">

                        {{ $transaction->asset->serial_number }}           
                 </td>
                  <td class="align-middle">{{$transaction->borrowDate}}</td>
                  <td class="align-middle">{{$transaction->returnDate}}</td>
             
                  

                  

                  {{-- <td colspan="4" class="text-center align-middle" >
                     <div class="d-inline-block">
                          <form method="POST" action="/transactions/{{$transaction->id}}">
                              @csrf
                              @method('PUT')
                             
                              <button type="submit" class="btn btn-danger" name="cancel" value="cancel">Cancel</button>
                            
                          </form>
                      </div>

                  </td> --}}
              </tr>
              @endif
         @endforeach
        </tbody>
      </table>
     {{-- {{ $transactions->render() }} --}}

      </div>

    {{-- All Transactions of non-admin user --}}


      <div class="card-header col-md-12">
          <h5>My All Transactions</h5>
      </div>

      <div class="card-body row col-md-12 custyle">
      <table class="table table-striped custab">
      <thead>
      
          <tr>
              <th class="text-center align-middle">Transaction No.</th>
              <th class="align-middle">Status</th>
              <th class="align-middle">Product Line</th>
              {{-- <th class="text-center align-middle">Asset Serial No.</th> --}}

           {{--    <th class="align-middle">Asset Serial No.</th> --}}
              <th class="align-middle">Borrow Date</th>
              <th class="align-middle">Return Date</th>
              
             {{--  <th class="text-center align-middle" colspan="2">Action</th> --}}
          </tr>
      </thead>
      <tbody>
         @foreach($transactions as $transaction)
            @if($transaction->status_id == 3 || $transaction->status_id == 4)
              <tr>
                 <td class="text-center align-middle"><a href="/transactions/{{$transaction->id}}">{{$transaction->id}}</a></td>
                 <td class="align-middle">{{$transaction->status->name}}</td>
                  <td class="align-middle">{{$transaction->category->name}}</td>

{{--                   <td class="text-center align-middle">
                      @if($transaction->status_id == 3)
                          {{ $na['na'] }}
                      @elseif($transaction->status_id == 4)
                          {{ $transaction->asset->serial_number }}
                      @endif

                  </td --}}
                 {{--  <td class="text-center align-middle"></td> --}}
                  <td class="align-middle">{{$transaction->borrowDate}}</td>
                  <td class="align-middle">{{$transaction->returnDate}}</td>
             
                  

                  

                  {{-- <td colspan="4" class="text-center align-middle" >
                     <div class="d-inline-block">
                          <form method="POST" action="/transactions/{{$transaction->id}}">
                              @csrf
                              @method('PUT')
                             
                              <button type="submit" class="btn btn-danger">Cancel</button>
                            
                          </form>
                      </div>

                  </td> --}}
              </tr>
            @endif
         @endforeach
        </tbody>
      </table>
     {{-- {{ $transactions->render() }} --}}

      </div>

@endcan    

@endsection