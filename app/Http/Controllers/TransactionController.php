<?php

namespace App\Http\Controllers;

use App\Transaction;
use App\Asset;
use App\Category;
use Illuminate\Http\Request;
use Auth;

class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->role_id === 1){

            $transactions = Transaction::orderBy('updated_at', 'desc')->get();
                                     /*$this->authorize('create', Transaction::class);*/
           /* dd($transactions);*/

       /*     $pending_transactions = Transaction::where('status_id', 1)->get();
            $approved_transactions =Transaction::where('status_id', 2)->get();
            $completed_transactions =Transaction::where('status_id', 5)->orWhere('status_id', 4)->get();*/

            $na = [
                'na' => 'N/A'
            ];
                                    /* $pending_transactions = Transaction::orderBy('created_at', 'desc')->paginate(4);

                                    $approved_transactions = Transaction::where('status_id', 2)->paginate(4);*/

            /*return view('transactions.index', compact('pending_transactions', 'approved_transactions', 'completed_transactions', 'na'));*/

            return view('transactions.index', compact('transactions', 'na'));
        }else{
            $transactions = Transaction::where('user_id', Auth::user()->id)->orderBy('updated_at', 'desc')->get();
            
            return view('transactions.index')->with('transactions', $transactions);

        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $borrowDate = $request->input('borrowDate');
        $returnDate = $request->input('returnDate');

        if($borrowDate && $returnDate){
           $transaction = new Transaction;

           $transaction->user_id = Auth::user()->id;


           $transaction->status_id = 1;
           $transaction->category_id = $request->input('category_id');
           $transaction->borrowDate = $request->input('borrowDate');
           $transaction->returnDate = $request->input('returnDate');

           $transaction->save();

           return back()->with('success', 'Successfully created the request. Please check transactions.');

        }else{
            return back()->with('warning', 'Please choose borrow and return dates');
        }
        



    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function show(Transaction $transaction)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function edit(Transaction $transaction)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Transaction $transaction)
    {
        $cancel = $request->input('cancel');
        if($cancel){
            if($transaction->status_id != 1){ // if user wants to cancel but admin already approved or denied request

                return back()->with();
            }else{
                $transaction->status_id = 3;

                
                $transaction->save();

                return back();
            }
 
        }
        


    

        $this->authorize('update', Transaction::class);
        $denied = $request->input('deny'); //get the value of the input element button deny
        $approved = $request->input('approve'); //get the value of the input element button approve
        $returned = $request->input('return'); //get the value of the input element button tag as returned

        /*dd($approved);*/

        if($denied){ //if deny button is clicked
            if($transaction->status_id != 1){
                //this is what is going to happen if admin tries to deny request but non admin already cancelled the request
            }else{
                $transaction->status_id = 5;
            }
                

        }elseif($returned){ // if tag as returned button is clicked
            $transaction->status_id = 4; // change status to "returned"
            $transaction->asset->isAvailable = 1; // make particular asset available
           /* dd($transaction->asset->isAvailable);*/
            $transaction->asset->save(); //save to database  // cant save to database yet
           /* dd($transaction->asset->isAvailable);*/
           /*dd($asset);*/   //"undefined variable asset" error 
        }elseif($approved){ //if approve button is clicked
               /* dd($transaction->id);*/
            if($transaction->status_id != 1){
                // this is what is going to happen if admin tries to approve request but non admin already cancelled it

                return redirect('/transactions');
            }else{
                
                 $transaction->status_id = 2; // change transaction status to "approved"
                 $asset = Asset::where('isAvailable', 1)->where('category_id', $transaction->category_id)->get()->first(); // get the particular asset object which is available and same category id in the transactions table
                /* dd($asset);*/


            }

           if($asset != null){ // if there is available asset or serial number of a particular category
                $transaction->asset_id = $asset->id; //save the asset_id to the transaction table
                $asset->isAvailable = 0; // update the isAvailble column of the asset to unavailable or 0

                $asset->save(); // save asset table  // 
           }else{ //if no available asset or serial number
                /*$category = Category::where('id', $transaction->category_id)->get()->first();*/                                                  // get the particular category object which has no available assets
                
               /* $category->isActive = 0;*/                               //deactivate the particular category object
                /*$category->save(); */                                   //save to the database
                return redirect('/transactions');

           }
            
        }else{
            //no button is clicked 
        }

        $transaction->save(); 

        return redirect('/transactions');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function destroy(Transaction $transaction)
    {
        //
    }
}
